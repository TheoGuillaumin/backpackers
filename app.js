let fs = require('fs');
let http = require('http');
let url = require('url');
let cookie = require('cookie');
let njwt = require('njwt');
let bcrypt = require('bcrypt')
let secureRandom = require('secure-random')
let remedial = require('remedial');
let arrayBook = fs.readFileSync('json/book.json');
let jsonBook = JSON.parse(arrayBook);
var arrayForum = fs.readFileSync('json/forum.json');
var jsonForum = JSON.parse(arrayForum);
let arrayUser = fs.readFileSync('json/user.json');
let jsonUser = JSON.parse(arrayUser);
let arrayCountView = fs.readFileSync('json/countViewTopics.json');
let jsonCountView = JSON.parse(arrayCountView);
let arrayMessageTopic = fs.readFileSync('json/messageTopics.json');
let jsonMessageTopic = JSON.parse(arrayMessageTopic);
let arrayMessageNewsletter = fs.readFileSync('json/newsletter.json');
let jsonMessageNewsletter = JSON.parse(arrayMessageNewsletter);
let nodemailer = require('nodemailer');
const { toUnicode } = require('punycode');
const KEYSIGN = secureRandom(256, { type: 'Buffer' });
const JWT_PROP = 'token';
const JWT_PROP_SIGNUP = 'token_validate_signUp';
const JWT_PROP_VALIDATE = 'token_validate_password';


/*Function that verify the presence of a token*/
function getToken(req, jwt) {
    try {
        var cookies = cookie.parse(req.headers.cookie || '');
        if (cookies && cookies[jwt]) {
            var verifiedJwt = njwt.verify(cookies[jwt], KEYSIGN);
            console.log('Il y a un token dans le cookie !');
            return true;
        } else {
            console.log('Pas de token dans le cookie !');
            return false;
        }

    } catch (error) {
        console.log(error);
        return false;
    }
}

/*function that search the folders name according to the conditions and then the filename asked further in the code by the URL*/
function sendFile(folder, fileName, res) {
    try {
        res.statusCode = 200;
        if (folder == "css") {
            res.setHeader('Content-type', 'text/css');
            let cssFile = fs.readFileSync('./css/' + fileName);
            res.end(cssFile);
        } else if (folder == "logos") {
            res.setHeader('Content-type', 'image/png');
            let imageFile = fs.readFileSync('./src/logos/' + fileName);
            res.end(imageFile);
        } else if (folder == "couverture") {
            res.setHeader('Content-type', 'image/png');
            let imageFile = fs.readFileSync('./src/couverture/' + fileName);
            res.end(imageFile);
        } else if (folder == "profile") {
            res.setHeader('Content-type', 'image/jpg');
            let imageFile = fs.readFileSync('./src/profile/' + fileName);
            res.end(imageFile);
        } else if (folder == "src") {
            res.setHeader('Content-type', 'image/jpg');
            let imageFile = fs.readFileSync('./src/' + fileName);
            res.end(imageFile);
        } else if (folder == "gallery") {
            res.setHeader('Content-type', 'image/jpg');
            let imageFile = fs.readFileSync('./src/gallery/' + fileName);
            res.end(imageFile);
        } else if (folder == "js") {
            res.setHeader('Content-type', 'application/javascript');
            let jsFile = fs.readFileSync('./js/' + fileName);
            res.end(jsFile);
        } else if (folder == "pages") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./pages/' + fileName);
            res.end(htmlFile);
        } else if (folder == "home") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./admin/home/' + fileName);
            res.end(htmlFile);
        } else if (folder == "biography") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./admin/biography/' + fileName);
            res.end(htmlFile);
        } else if (folder == "books") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./admin/books/' + fileName);
            res.end(htmlFile);
        } else if (folder == "faq") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./admin/faq/' + fileName);
            res.end(htmlFile);
        } else if (folder == "forum") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./admin/forum/' + fileName);
            res.end(htmlFile);
        } else if (folder == "galery") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./admin/galery/' + fileName);
            res.end(htmlFile);
        } else if (folder == "users") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./admin/users/' + fileName);
            res.end(htmlFile);
        } else if (folder == "newsletter") {
            res.setHeader('Content-type', 'text/html');
            let htmlFile = fs.readFileSync('./admin/newsletter/' + fileName);
            res.end(htmlFile);
        } else if (folder == "templateJS") {
            res.setHeader('Content-type', 'application/javascript');
            let jsFile = fs.readFileSync('./admin/templateJS/' + fileName);
            res.end(jsFile);
        } else if (folder == "Style") {
            res.setHeader('Content-type', 'text/css');
            let cssFile = fs.readFileSync('./admin/Style/' + fileName);
            res.end(cssFile);
        } else if (folder == "icons") {
            res.setHeader('Content-type', 'image/png');
            let imageFile = fs.readFileSync('./src/icons/' + fileName);
            res.end(imageFile);
        } else if (folder == "favicon") {
            res.setHeader('Content-type', 'image/png');
            let imageFile = fs.readFileSync('./src/favicon/' + fileName);
            res.end(imageFile);
        } else {
            throw "wrong folder";
        }
    } catch (e) {
        console.log(e);
        res.writeHead(404);
        res.end();
    }
}

/*Creation of the http server*/
const serveur = http.createServer((req, res) => {
    console.log("url demandé : " + req.url);
    /* Split the URL to identify the path */
    let monUrl = url.parse(req.url, true);
    let tabUrl = req.url.split('/');
    console.log(tabUrl);
    /* Take the query in the URL to use them later*/
    let idUrl = monUrl.query.id;
    let emailUrl = monUrl.query.email;
    let nameUrl = monUrl.query.name;
    let mailUrl = monUrl.query.mail;
    let lastNameUrl = monUrl.query.name;
    let firstNameUrl = monUrl.query.firstName;
    let pseudoUrl = monUrl.query.pseudo;
    let passwordUrl = monUrl.query.password;
    let passwordConfUrl = monUrl.query.passwordConf;
    let textUrl = monUrl.query.mailText;
    let catUrl = monUrl.query.cat;
    let titleUrl = monUrl.query.title;
    let subjectNewsletterUrl = monUrl.query.nssubject;
    let titleNewsletterUrl = monUrl.query.nstitle;
    let contentNewsletterUrl = monUrl.query.nscontent;
    let msgUrl = monUrl.query.msg;
    let titleTopicUrl = monUrl.query.titletopic;

    /*Switch for the special case like page with id or everything concerning mail,sign in and sign up*/
    switch (monUrl.pathname) {

        case '/':
        case '/index.html':
            /*Case concerning the newsletter inscription button presence in the index page*/
        case '/pages/index.html':
            let viewSignUpNewsletter = true;
            if (getToken(req, JWT_PROP)) {
                console.log('Connecter');
                for (let i = 0; i < jsonUser.user.length; i++) {
                    var userMailNewsletterExist = jsonUser.user[i].userMail;
                    for (let i = 0; i < jsonMessageNewsletter.mails.length; i++) {
                        var mailUserNewsletter = jsonMessageNewsletter.mails[i].mail;
                        if (userMailNewsletterExist == mailUserNewsletter) {
                            console.log('Cacher la newsletter');
                            viewSignUpNewsletter = false;
                            let patchIndex = {};
                            patchIndex["signUpNewsletter"] = '';

                            let pageIndex3 = fs.readFileSync("pages/index.html", "UTF-8");
                            pageIndex3 = pageIndex3.supplant(patchIndex);

                            res.writeHead(200, { 'Content-Type': 'text/html' });
                            res.write(pageIndex3);
                            res.end();
                        }
                    }
                }
                if (viewSignUpNewsletter) {
                    console.log('Afficher la newsletter');
                    let patchIndex = {};
                    patchIndex["signUpNewsletter"] = '<section class="textCenter"><div class="sectionSeperator"></div><h3 id="newsletterTitle">S\'inscrire à la newsletter</h3><form action="traitementSignUpNewsletter.html" method="GET" id="newsletterSub"><input type="email" name="mail" id="newsletterSubInput" placeholder="Entrez votre adresse mail" required><button type="submit" id="newsletterSubButton">Valider</button></form></section>';

                    let pageIndex3 = fs.readFileSync("pages/index.html", "UTF-8");
                    pageIndex3 = pageIndex3.supplant(patchIndex);

                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(pageIndex3);
                    res.end();
                }
            } else {
                console.log('Pas connecter');
                let patchIndex = {};
                patchIndex["signUpNewsletter"] = '<section class="textCenter"><div class="sectionSeperator"></div><h3 id="newsletterTitle">S\'inscrire à la newsletter</h3><form action="traitementSignUpNewsletter.html" method="GET" id="newsletterSub"><input type="email" name="mail" id="newsletterSubInput" placeholder="Entrez votre adresse mail" required><button type="submit" id="newsletterSubButton">Valider</button></form></section>';

                let pageIndex3 = fs.readFileSync("pages/index.html", "UTF-8");
                pageIndex3 = pageIndex3.supplant(patchIndex);

                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pageIndex3);
                res.end();
            }
            break;

            /*Case concerning the book pages loading with an ID */
        case '/pages/book.html':

            let patchBook = {};
            patchBook["name"] = jsonBook.books[idUrl].name;
            patchBook["img"] = jsonBook.books[idUrl].img;
            patchBook["description"] = jsonBook.books[idUrl].description;
            patchBook["link"] = jsonBook.books[idUrl].link;
            patchBook["button"] = jsonBook.books[idUrl].button;
            patchBook["commentName1"] = jsonBook.books[idUrl].commentName1;
            patchBook["comment1"] = jsonBook.books[idUrl].comment1;
            patchBook["commentName2"] = jsonBook.books[idUrl].commentName2;
            patchBook["comment2"] = jsonBook.books[idUrl].comment2;
            patchBook["commentName3"] = jsonBook.books[idUrl].commentName3;
            patchBook["comment3"] = jsonBook.books[idUrl].comment3;
            patchBook["commentTitle"] = '<h5 class="fontStyle">Commentaires</h5>';
            for (let i = 1; i <= 3; i++) {
                var commentName = 'jsonBook.books[' + idUrl + '].commentName' + i;
                var comment = 'jsonBook.books[' + idUrl + '].comment' + i;
                var commentNamePatch = 'commentName' + i;
                var commentPatch = 'comment' + i;
                var resultNameComment = 'patchBook["' + commentNamePatch + '"] = \'\';';
                var resultComment = 'patchBook["' + commentPatch + '"] = \'Il n’y a pas suffisamment de commentaires\';';
                if (eval(comment) == "" || eval(commentName) == "") {
                    eval(resultNameComment);
                    eval(resultComment);
                }
            }
            let pageBook = fs.readFileSync("pages/book.html", "UTF-8");
            pageBook = pageBook.supplant(patchBook);

            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(pageBook);
            res.end();

            break;

            /*Case concerning the forum pages loading differents categories dynamically*/
        case '/pages/forum.html':

            let patchForum = {};
            let patchForumSection = {};
            let patchForumCategory = {};
            let patchForumSubCategory = {};

            let forumSection = '';
            for (let i = 0; i < jsonForum.arrayCategory.length; i++) {
                forumSection += '{forumCategory' + i + '}';
                console.log(forumSection);
            }
            let forumView = 'patchForum["forumCategory"] = \'' + forumSection + '\'';
            eval(forumView);

            for (let i = 0; i < jsonForum.arrayCategory.length; i++) {
                let forum = 'patchForumSection["forumCategory' + i + '"] = \'<!-- Forum Category ' + i + ' --><section class="boxCategory"><div class="headerCategory flexForum"><h3 class="fontStyle">{nameCategory' + i + '}</h3><h3 class="fontStyle">Sujets</h3></div><div>{subCategory' + i + '}</div></section>\';';
                eval(forum);
                let forumCategory = 'patchForumCategory["nameCategory' + i + '"] = jsonForum.arrayCategory[' + i + '].name;';
                eval(forumCategory);
                let forumSubCategory = 'patchForumSubCategory["subCategory' + i + '"] = \'\';';
                eval(forumSubCategory);

                let forumSubCategoryLength = 'jsonForum.arraySubCategory' + i + '.length';

                for (let j = 0; j < eval(forumSubCategoryLength); j++) {
                    let nbTopics = 'jsonForum.arraySub' + i + '' + j + '.length';
                    if (j == 0) {
                        let forumCategoryView = 'patchForumSubCategory["subCategory' + i + '"] += \'<a href="subCategory.html?id=' + i + '' + j + '&cat=' + i + '&title=' + j + '" class="flexForum sub"><div class="subCategoryInformation"><h4 class="fontStyle">{nameCategoryNameSubCategory' + i + '' + j + '}</h4><p class="fontStyle">{descriptionCategoryNameSubCategory' + i + '' + j + '}</p></div><div class="topic fontStyle"><p>' + eval(nbTopics) + '</p></div></a>\';';
                        eval(forumCategoryView);
                    } else {
                        let forumCategoryView = 'patchForumSubCategory["subCategory' + i + '"] += \'<div class="separatorSub"></div><a href="subCategory.html?id=' + i + '' + j + '&cat=' + i + '&title=' + j + '" class="flexForum sub"><div class="subCategoryInformation"><h4 class="fontStyle">{nameCategoryNameSubCategory' + i + '' + j + '}</h4><p class="fontStyle">{descriptionCategoryNameSubCategory' + i + '' + j + '}</p></div><div class="topic fontStyle"><p>' + eval(nbTopics) + '</p></div></a>\';';
                        eval(forumCategoryView);
                    }
                }

                let varForumSubCategory = 'patchForumSubCategory' + i + ' = {};';
                eval(varForumSubCategory);

                for (let j = 0; j < eval(forumSubCategoryLength); j++) {
                    let forumCategoryNameSubCategory = 'patchForumSubCategory' + i + '["nameCategoryNameSubCategory' + i + '' + j + '"] = jsonForum.arraySubCategory' + i + '[' + j + '].name;';
                    eval(forumCategoryNameSubCategory);
                    let forumDescriptionCategoryNameSubCategory = 'patchForumSubCategory' + i + '["descriptionCategoryNameSubCategory' + i + '' + j + '"] = jsonForum.arraySubCategory' + i + '[' + j + '].description;';
                    eval(forumDescriptionCategoryNameSubCategory);
                }

            }

            let pageForum = fs.readFileSync("pages/forum.html", "UTF-8");
            pageForum = pageForum.supplant(patchForum);
            pageForum = pageForum.supplant(patchForumSection);
            pageForum = pageForum.supplant(patchForumCategory);
            pageForum = pageForum.supplant(patchForumSubCategory);
            for (let i = 0; i < jsonForum.arrayCategory.length; i++) {
                let supplantForumSubCategory = 'pageForum = pageForum.supplant(patchForumSubCategory' + i + ');';
                eval(supplantForumSubCategory);
            }

            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(pageForum);
            res.end();

            break;

            /*Case concerning the load of the different subCategories with different ID */
        case "/pages/subCategory.html":

            let patchSubCategoryTopic = {};
            let patchSubCategoryContent = {};
            let patchSubCategoryAdd = {};
            patchSubCategoryTopic["subCategoryContent"] = '';
            let patchSubCategoryContentAdd = 'patchSubCategoryContent["subCategoryTitle"] = jsonForum.arraySubCategory' + catUrl + '[' + titleUrl + '].name;';
            eval(patchSubCategoryContentAdd);

            let idJsonLength = 'jsonForum.arraySub' + idUrl + '.length';

            for (let i = 0; i < eval(idJsonLength); i++) {

                let countTopicView = 'jsonCountView.countView' + idUrl + '[' + i + '].count';
                fs.writeFileSync('./json/countViewTopics.json', JSON.stringify(jsonCountView), 'UTF-8');

                let dateMessageTopicLength = 'jsonMessageTopic.messageTopic' + idUrl + '' + i + '.length';
                let iDateMessageTopic = eval(dateMessageTopicLength) - 1;
                let dateMessageTopic = 'jsonMessageTopic.messageTopic' + idUrl + '' + i + '[' + iDateMessageTopic + '].dateAdd';
                let splitDate = eval(dateMessageTopic).split('/');
                myDate = new Date();
                myYear = myDate.getFullYear();
                myMonth = myDate.getMonth() + 1;
                myDay = myDate.getDate();
                myHours = myDate.getHours();
                myMinutes = myDate.getMinutes();

                let viewDate = [];

                viewDateMinutesMath = myMinutes - eval(splitDate[4]);
                viewDateMinutes = (viewDateMinutesMath < 0) ? viewDateMinutesMath * -1 : viewDateMinutesMath;
                if (!(viewDateMinutes == 0)) {
                    viewDate.push(viewDateMinutes + 'min');
                    console.log(viewDate);

                    viewDateHoursMath = myHours - eval(splitDate[3]);
                    viewDateHours = (viewDateHoursMath < 0) ? viewDateHoursMath * -1 : viewDateHoursMath;
                    if (!(viewDateHours == 0)) {
                        viewDate.push(viewDateHours + 'h');
                        console.log(viewDate);

                        viewDateDayMath = myDay - eval(splitDate[0]);
                        viewDateDay = (viewDateDayMath < 0) ? viewDateDayMath * -1 : viewDateDayMath;
                        if (!(viewDateDay == 0)) {
                            viewDate.push(viewDateDay + 'j');
                            console.log(viewDate);

                            viewDateMonthMath = myMonth - eval(splitDate[1]);
                            viewDateMonth = (viewDateMonthMath < 0) ? viewDateMonthMath * -1 : viewDateMonthMath;
                            if (!(viewDateMonth == 0)) {
                                viewDate.push(viewDateMonth + 'm');
                                console.log(viewDate);

                                viewDateYearMath = myYear - eval(splitDate[2]);
                                viewDateYear = (viewDateYearMath < 0) ? viewDateYearMath * -1 : viewDateYearMath;
                                if (!(viewDateYear == 0)) {
                                    if (viewDateYear == 1) {
                                        viewDate.push(viewDateYear + 'an');
                                        console.log(viewDate);
                                    } else {
                                        viewDate.push(viewDateYear + 'ans');
                                        console.log(viewDate);
                                    }

                                }

                            }

                        }

                    }

                } else {
                    viewDate = '...';
                }

                let nbLastMessage = viewDate.length - 1;

                let jsonMessageTopicLength = 'jsonMessageTopic.messageTopic' + idUrl + '' + i + '.length';
                if (i == 0) {
                    patchSubCategoryTopic["subCategoryContent"] += '<a href="topic.html?id=' + idUrl + '&title=' + i + '" class="flexForum sub"><h4 class="fontStyle">{topicTitle' + idUrl + '' + i + '}</h4><div class="rightInformation"><p class="reply fontStyle">' + eval(jsonMessageTopicLength) + '</p><p class="view fontStyle">' + eval(countTopicView) + '</p><p class="activity fontStyle">' + viewDate[nbLastMessage] + '</p></div></a>';
                } else {
                    patchSubCategoryTopic["subCategoryContent"] += '<div class="separatorSub"></div><a href="topic.html?id=' + idUrl + '&title=' + i + '" class="flexForum sub"><h4 class="fontStyle">{topicTitle' + idUrl + '' + i + '}</h4><div class="rightInformation"><p class="reply fontStyle">' + eval(jsonMessageTopicLength) + '</p><p class="view fontStyle">' + eval(countTopicView) + '</p><p class="activity fontStyle">' + viewDate[nbLastMessage] + '</p></div></a>';
                }

                let topicTitle = 'patchSubCategoryContent["topicTitle' + idUrl + '' + i + '"] = jsonForum.arraySub' + idUrl + '[' + i + '].title;';
                eval(topicTitle);
                let topicNbAnswer = 'patchSubCategoryContent["topicNbAnswer' + idUrl + '' + i + '"] = jsonForum.arraySub' + idUrl + '[' + i + '].nbAnswer;';
                eval(topicNbAnswer);
            }

            if (getToken(req, JWT_PROP)) {
                patchSubCategoryAdd["subCategoryAddTopics"] = '<form action="subCategory.html" method="GET"><div class="topicMessageReply"><div class="flexTopic flexTopicCenter"><h5 class="fontStyle">Message</h5></div><input type="hidden" name="id" value="' + idUrl + '"><input type="hidden" name="cat" value="' + catUrl + '"><input type="hidden" name="title" value="' + titleUrl + '"><div class="flexTopic flexTopicCenter"><textarea name="titletopic" cols="30" rows="10" placeholder="Entrez ici votre message." required></textarea></div><div class="flexTopic flexTopicCenter"><button type="submit">Poser ma question</button></div></div></form>';
            } else {
                patchSubCategoryAdd["subCategoryAddTopics"] = '';
            }

            let pageSubCategory = fs.readFileSync("pages/subCategory.html", "UTF-8");

            pageSubCategory = pageSubCategory.supplant(patchSubCategoryTopic);
            pageSubCategory = pageSubCategory.supplant(patchSubCategoryContent);
            pageSubCategory = pageSubCategory.supplant(patchSubCategoryAdd);

            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(pageSubCategory);
            res.end();
            break;


            /*Case concerning the load of the different topics with different ID */
        case "/pages/topic.html":

            if (getToken(req, JWT_PROP)) {
                if (idUrl && titleUrl && msgUrl) {
                    myDate = new Date();
                    myDateAdd = myDate.getDate() + '/' + eval(myDate.getMonth() + 1) + '/' + myDate.getFullYear() + '/' + myDate.getHours() + '/' + myDate.getMinutes();
                    let msg = {
                        "pseudo": "John MacKoney",
                        "imgProfile": "1",
                        "message": msgUrl,
                        "dateAdd": myDateAdd
                    };
                    let msgAdd = 'jsonMessageTopic.messageTopic' + idUrl + '' + titleUrl + '.push(' + JSON.stringify(msg) + ')';
                    eval(msgAdd);
                    fs.writeFileSync('./json/messageTopics.json', JSON.stringify(jsonMessageTopic), 'UTF-8');
                }
            }

            let countTopicView = 'jsonCountView.countView' + idUrl + '[' + titleUrl + '].count';
            let calculCountView = eval(countTopicView) + ' + 1';
            let addCountTopicView = eval(calculCountView);
            let countTopicView2 = 'jsonCountView.countView' + idUrl + '[' + titleUrl + '].count = \'' + addCountTopicView + '\'';
            eval(countTopicView2);

            let patchTopic = {};
            let patchMessagesTopic = {};
            let patchTopicAddMessages = {};

            let topicTitle = 'patchTopic["topicTitle"] = jsonForum.arraySub' + idUrl + '[' + titleUrl + '].title';
            eval(topicTitle);

            let iMessageTopicLength = 'jsonMessageTopic.messageTopic' + idUrl + '' + titleUrl + '.length';

            let topicMessages = '';
            for (let i = 0; i < eval(iMessageTopicLength); i++) {
                topicMessages += '<div class="topicMessage"><div class="flexTopic flexTopicStart userMessageInformation"><img src="../src/profile/{topicImgProfile' + i + '}" alt="nom"><p class="fontStyle name">{topicPseudo' + i + '}</p></div><div class="flexTopic flexTopicStart"><p class="fontStyle message">{topicMessages' + i + '}</p></div><div class="flexTopic flexTopicSpaceBetween"><p class="fontStyle date">{topicDateAdd' + i + '}</p><p class="fontStyle like">12 <i class="fas fa-thumbs-up like"></i></p></div></div>';
                console.log(topicMessages);
            }
            let topicView = 'patchTopic["TopicMessages"] = \'' + topicMessages + '\'';
            eval(topicView);

            for (let i = 0; i < eval(iMessageTopicLength); i++) {

                let topicPseudo = 'patchMessagesTopic["topicPseudo' + i + '"] = jsonMessageTopic.messageTopic' + idUrl + '' + titleUrl + '[' + i + '].pseudo';
                eval(topicPseudo);
                let topicImgProfile = 'patchMessagesTopic["topicImgProfile' + i + '"] = jsonMessageTopic.messageTopic' + idUrl + '' + titleUrl + '[' + i + '].imgProfile';
                eval(topicImgProfile);
                let topicMessages = 'patchMessagesTopic["topicMessages' + i + '"] = jsonMessageTopic.messageTopic' + idUrl + '' + titleUrl + '[' + i + '].message';
                eval(topicMessages);

                let patchTopicDateAdd = 'jsonMessageTopic.messageTopic' + idUrl + '' + titleUrl + '[' + i + '].dateAdd';
                eval(patchTopicDateAdd);
                let splitDate = eval(patchTopicDateAdd).split('/');
                let topicDateSplit = splitDate[0] + '/' + splitDate[1] + '/' + splitDate[2];
                let topicDateAdd = 'patchMessagesTopic["topicDateAdd' + i + '"] = \'' + topicDateSplit + '\'';
                eval(topicDateAdd);
            }

            if (getToken(req, JWT_PROP)) {
                patchTopic["topicAddMessages"] = '<form action="topic.html" method="GET"><div class="topicMessageReply"><div class="flexTopic flexTopicCenter"><h5 class="fontStyle">Message</h5></div><input type="hidden" name="id" value="' + idUrl + '"><input type="hidden" name="title" value="' + titleUrl + '"><div class="flexTopic flexTopicCenter"><textarea name="msg" cols="30" rows="10" placeholder="Entrez ici votre message." required></textarea></div><div class="flexTopic flexTopicCenter"><button type="submit">Répondre</button></div></div></form>';
            } else {
                patchTopic["topicAddMessages"] = '';
            }


            let pageTopic = fs.readFileSync("pages/topic.html", "UTF-8");

            pageTopic = pageTopic.supplant(patchTopic);
            pageTopic = pageTopic.supplant(patchMessagesTopic);
            pageTopic = pageTopic.supplant(patchTopicAddMessages);

            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(pageTopic);
            res.end();
            break;

            /*Case for sending mail in the contact page */
        case '/pages/sendMail.html':

            function mail(mailForm, mailName, mailContent) {
                let transport = nodemailer.createTransport({
                    host: 'smtp.mailtrap.io',
                    port: 2525,
                    auth: {
                        user: '1faaddce74adff',
                        pass: '65b09b8502a6ca'
                    }
                });
                let message = {
                    from: 'contact@email.com',
                    to: mailForm,
                    subject: 'Backpackers - Contact',
                    html: '<div style="text-align:center;"><strong>Nom :</strong> ' + mailName + '</div><div style="text-align:center;"><strong>Email :</strong> ' + mailForm + '</div><div style="text-align:center;"><strong>Contenu :</strong> ' + mailContent + '</div>'
                };
                transport.sendMail(message, function(err, info) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(info);
                    }
                });
                console.log('ok');
            }
            mail(emailUrl, nameUrl, textUrl);

            let pageMail = fs.readFileSync("pages/sendMail.html");

            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(pageMail);
            res.end();
            break;

            /*Case concerning the newsletter in the admin panel */
        case '/admin/newsletter/newsletterConfirmation.html':

            function newsletter() {

                let mailList = '';
                for (let i = 0; i < jsonMessageNewsletter.mails.length; i++) {
                    if (i == 0) {
                        mailList += jsonMessageNewsletter.mails[i].mail;
                    } else {
                        mailList += ', ' + jsonMessageNewsletter.mails[i].mail;
                    }
                }
                console.log('Mails : ' + mailList);

                let transport = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'poiroujulien@gmail.com',
                        pass: 'totorledur'
                    }
                });

                let mailOptions = {
                    from: 'poiroujulien@gmail.com',
                    bcc: mailList,
                    subject: 'Backpackers - ' + subjectNewsletterUrl,
                    html: '<div style="text-align:center;"><img src="http://localhost:8080/src/logos/logo_site.png" alt="Logo"></div><br/><div style="text-align:center;"><strong>Le monde des Backpackers</strong></div><div style="text-align:center;">' + titleNewsletterUrl + '</div><br/><div style="text-align:center;">' + contentNewsletterUrl + '</div>'
                };

                transport.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });
            }

            newsletter();

            let pageNewsletter = fs.readFileSync("admin/newsletter/newsletterConfirmation.html");

            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(pageNewsletter);
            res.end();
            break;

            /*Case concerning the sign up part in the connection page*/
        case '/pages/traitementSignUp.html':
            let pageSignUp = fs.readFileSync("pages/traitementSignUp.html");
            user = {
                "userMail": mailUrl,
                "userLastName": lastNameUrl,
                "userFirstName": firstNameUrl,
                "userPseudo": pseudoUrl,
                "userPassword": passwordUrl,
                "userValidate": "0"
            };

            var comparaison;
            for (i = 0; i < jsonUser.user.length; i++) {
                let mail2 = jsonUser.user[i].userMail;
                let pseudo = jsonUser.user[i].userPseudo;

                if (mail2 == mailUrl || pseudo == pseudoUrl) {
                    comparaison = false;
                    break;
                } else {
                    comparaison = true;
                }
            }
            if (passwordUrl == passwordConfUrl && comparaison == true) {
                jsonUser.user.push(user);
                fs.writeFileSync('./json/user.json', JSON.stringify(jsonUser), 'UTF-8');

                function createTokenForUser(rUser, rProfil) {
                    let claims = {
                        iss: 'http://localhost:8080',
                        sub: rUser,
                        scope: rProfil
                    };
                    return njwt.create(claims, KEYSIGN);
                }

                function writeTokenCookie(res, token_validate_signUp) {
                    res.writeHead(200, {
                        'Content-Type': 'text/html',
                        'Set-Cookie': cookie.serialize(JWT_PROP_SIGNUP, token_validate_signUp, {
                            httpOnly: true,
                            maxAge: 60 * 60 * 24 * 7 // 1 Week
                        })
                    });
                    res.write(pageSignUp);
                    res.end();
                }
                var token_validate_signUp = createTokenForUser(mailUrl, "USER");
                writeTokenCookie(res, token_validate_signUp);

                function confMail() {
                    let transport = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'poiroujulien@gmail.com',
                            pass: 'totorledur'
                        }
                    });
                    let mailOptions = {
                        from: 'poiroujulien@gmail.com',
                        to: user.userMail,
                        subject: 'Backpackers - activation du compte',
                        html: '<div style="text-align:center;"><img src="http://localhost:8080/src/logos/logo_site.png" alt="Logo"></div><br/><div style="text-align:center;"><strong>Le monde des Backpackers</strong></div><div style="text-align:center;">' + "Veuillez confirmer la création de votre compte" + '<div><a href = "http://localhost:8080/pages/validationCompte.html">Validation de votre compte</a></div>'
                    };

                    transport.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log('Email sent: ' + info.response);
                        }
                    });
                }

                confMail();

            } else {
                let pageNotCreated = fs.readFileSync("pages/accountNotCreated.html");
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pageNotCreated);
                res.end();
                break;
            }

            break;

            /*Case concerning the validation of the account*/
        case '/pages/traitementValidation.html':
            let pageConfirme = fs.readFileSync("pages/traitementValidation.html");
            let pageTraitementValidation = fs.readFileSync("pages/accountNotFound.html");
            let testLoadPageAccountNotValidated = true;



            for (i = 0; i < jsonUser.user.length; i++) {
                let mail = jsonUser.user[i].userMail;
                let password = jsonUser.user[i].userPassword;
                if (getToken(req, JWT_PROP_SIGNUP)) {
                    if (mail == mailUrl && password == passwordUrl) {
                        testLoadPageAccountNotValidated = false;
                        jsonUser.user[i].userValidate = '1';
                        fs.writeFileSync('./json/user.json', JSON.stringify(jsonUser), 'UTF-8');
                        res.writeHead(200, { 'Content-Type': 'text/html' });
                        res.write(pageConfirme);
                        res.end();
                    }
                }
            }
            if (testLoadPageAccountNotValidated) {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pageTraitementValidation);
                res.end();
            }


            break;


            /*Case concerning the connection*/
        case '/pages/traitementSignIn.html':

            let pageAccountNotFound = fs.readFileSync("pages/accountNotFound.html");
            let pageTraitementSignIn = fs.readFileSync("pages/traitementSignIn.html");
            let testLoadPageAccountNotFound = true;

            for (let i = 0; i < jsonUser.user.length; i++) {

                let mail = jsonUser.user[i].userMail;
                let password = jsonUser.user[i].userPassword;
                let userValidate = jsonUser.user[i].userValidate;

                if (mail == mailUrl && password == passwordUrl && userValidate == '1') {
                    testLoadPageAccountNotFound = false;

                    function createTokenForUser(rUser, rProfil) {
                        let claims = {
                            iss: 'http://localhost:8080',
                            sub: rUser,
                            scope: rProfil
                        };
                        return njwt.create(claims, KEYSIGN);
                    }

                    var token = createTokenForUser(mail, "USER");

                    function writeTokenCookie(res, token) {
                        res.writeHead(200, {
                            'Content-Type': 'text/html',
                            'Set-Cookie': cookie.serialize(JWT_PROP, token, {
                                httpOnly: true,
                                maxAge: 60 * 60 * 24 * 7 // 1 Week
                            })
                        });
                        res.write(pageTraitementSignIn);
                        res.end();
                    }

                    writeTokenCookie(res, token);

                }

            }

            if (testLoadPageAccountNotFound) {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pageAccountNotFound);
                res.end();
            }
            break;

            /*Case concerning the disconnection*/
        case '/pages/disconnection.html':

            let pageTraitementDisconnection = fs.readFileSync("pages/disconnection.html");
            let redirectPageSignIn = fs.readFileSync("pages/connexion.html");

            if (getToken(req, JWT_PROP)) {

                function writeTokenCookie(res, token) {
                    res.writeHead(200, {
                        'Content-Type': 'text/html',
                        'Set-Cookie': cookie.serialize(JWT_PROP, token, {
                            httpOnly: true,
                            maxAge: -1
                        })
                    });
                    res.write(pageTraitementDisconnection);
                    res.end();
                }

                writeTokenCookie(res, token);

            } else {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(redirectPageSignIn);
                res.end();
            }

            break;

            /*Case concerning the sign up for the newsletter*/
        case '/pages/traitementSignUpNewsletter.html':

            let testMailNoExist = true;
            let pageTraitementNewsletter = fs.readFileSync("pages/traitementSignUpNewsletter.html");
            let mailTest = mailUrl;
            let pageMailExist = fs.readFileSync("pages/mailExist.html");

            mailAdd = {
                "mail": mailUrl
            };

            console.log(mailAdd);

            for (let i = 0; i < jsonMessageNewsletter.mails.length; i++) {
                mailTest = jsonMessageNewsletter.mails[i].mail;
                if (mailTest == mailUrl) {
                    testMailNoExist = false;
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(pageMailExist);
                    res.end();
                }

            }

            if (testMailNoExist) {
                jsonMessageNewsletter.mails.push(mailAdd);
                fs.writeFileSync('./json/newsletter.json', JSON.stringify(jsonMessageNewsletter), 'UTF-8');
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pageTraitementNewsletter);
                res.end();
            }
            break;

            /*Case concerning the password forgotten functionality */
        case '/pages/traitementPasswordForgotten.html':

            let linkToResetPassword = 'http://localhost:8080/pages/formRequestNewPassword.html';
            let pagePasswordForgotten = fs.readFileSync("pages/traitementPasswordForgotten.html");
            let passwordForgotten = true;

            function createTokenForUser(rUser, rProfil) {
                let claims = {
                    iss: 'http://localhost:8080',
                    sub: rUser,
                    scope: rProfil
                };
                return njwt.create(claims, KEYSIGN);
            }

            function writeTokenCookie(res, token_validate_password) {
                res.writeHead(200, {
                    'Content-Type': 'text/html',
                    'Set-Cookie': cookie.serialize(JWT_PROP_VALIDATE, token_validate_password, {
                        httpOnly: true,
                        maxAge: 60 * 60 * 24 * 7 // 1 Week-
                    })
                });
                res.write(pagePasswordForgotten);
                res.end();
            }

            for (let i = 0; i < jsonMessageNewsletter.mails.length; i++) {
                let mailUser = jsonMessageNewsletter.mails[i].mail;
                if (mailUser == mailUrl) {
                    passwordForgotten = false;
                    var token_validate_password = createTokenForUser(mailUrl, "USER");
                    writeTokenCookie(res, token_validate_password);
                    console.log('Le mail existe');

                    function mail(mailForm) {
                        let transport = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'poiroujulien@gmail.com',
                                pass: 'totorledur'
                            }
                        });
                        let message = {
                            from: 'poiroujulien@gmail.com',
                            to: mailForm,
                            subject: 'Backpackers - Contact',
                            html: '<div style="text-align:center;"><strong>Réinitialisation du mot de passe</strong></div><div style="text-align:center;"><a href="' + linkToResetPassword + '">Réinitialiser</a></div>'
                        };
                        transport.sendMail(message, function(err, info) {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log(info);
                            }
                        });
                    }
                    mail(mailUrl);
                }
            }

            if (passwordForgotten) {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pagePasswordForgotten);
                res.end();
                console.log('Le mail n\'existe pas');
            }

            break;

            /*Case concerning the request of a non existent password*/
        case '/pages/traitementNoRequestPassword.html':

            let pageNoRequestPassword = fs.readFileSync("pages/traitementNoRequestPassword.html");
            let pageRequestPassword = fs.readFileSync("pages/formRequestNewPassword.html");

            if (getToken(req, JWT_PROP_VALIDATE)) {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pageRequestPassword);
                res.end();
            } else {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pageNoRequestPassword);
                res.end();
            }

            break;


            /*Case concerning the request of a new password*/
        case '/pages/traitementNewPassword.html':

            let pageTraitementNewPassword = fs.readFileSync("pages/traitementNewPassword.html");
            let pageRedirectPageSignIn = fs.readFileSync("pages/traitementNoNewPassword.html");
            let newPassword = true;

            if (getToken(req, JWT_PROP_VALIDATE)) {

                for (let i = 0; i < jsonUser.user.length; i++) {
                    let mailUser = jsonUser.user[i].userMail;
                    if (mailUser == mailUrl && passwordUrl == passwordConfUrl) {
                        newPassword = false;
                        jsonUser.user[i].userPassword = passwordUrl;
                        fs.writeFileSync('./json/user.json', JSON.stringify(jsonUser), 'UTF-8');

                        res.writeHead(200, {
                            'Content-Type': 'text/html',
                            'Set-Cookie': cookie.serialize(JWT_PROP_VALIDATE, token_validate_password, {
                                httpOnly: true,
                                maxAge: -1
                            })
                        });
                        res.write(pageTraitementNewPassword);
                        res.end();
                    }
                }

            }

            if (newPassword) {
                res.writeHead(200, {
                    'Content-Type': 'text/html',
                    'Set-Cookie': cookie.serialize(JWT_PROP_VALIDATE, token, {
                        httpOnly: true,
                        maxAge: -1
                    })
                });
                res.write(pageRedirectPageSignIn);
                res.end();
            }

            break;
    }


    /* Go trough the URL and search the URL asked for */
    if (tabUrl.length > 3) {
        var folder = tabUrl[2];
        var file = tabUrl[3];
    } else {
        var folder = tabUrl[1];
        var file = tabUrl[2];
    }

    sendFile(folder, file, res);

});

let portEcoute = 8080;
console.log('Ecoute sur le port %s', portEcoute);
/*loading of the server*/
serveur.listen(portEcoute);