/* Table JSON */

let array = [{
    "name": "Symbole du Laos",
    "category": "catCambodge1",
    "categoryType": "Cambodge",
    "src": "cam-aigle.jpg",
    "type": "Landscape"
}, {
    "name": "Cité d’Angkor",
    "category": "catCambodge2",
    "categoryType": "Cambodge",
    "src": "cam-angkor1.jpg",
    "type": "Landscape"
}, {
    "name": "Le Bayon",
    "category": "catCambodge3",
    "categoryType": "Cambodge",
    "src": "cam-angkor4.jpg",
    "type": "Landscape"
}, {
    "name": "Buffles d’eau",
    "category": "catCambodge4",
    "categoryType": "Cambodge",
    "src": "cam-buffle-d-eau2.jpg",
    "type": "Landscape"
}, {
    "name": "Enfants pêcheurs",
    "category": "catCambodge5",
    "categoryType": "Cambodge",
    "src": "cam-coucher-de-soleil.jpg",
    "type": "Landscape"
}, {
    "name": "Bateau de pêche",
    "category": "catCambodge6",
    "categoryType": "Cambodge",
    "src": "cam-ile-paradisiaque2.jpg",
    "type": "Landscape"
}, {
    "name": "Bon appétit !",
    "category": "catCambodge7",
    "categoryType": "Cambodge",
    "src": "cam-marche2.jpg",
    "type": "Landscape"
}, {
    "name": "Longue vie aux mariés !",
    "category": "catCambodge8",
    "categoryType": "Cambodge",
    "src": "cam-mariage.jpg",
    "type": "Landscape"
}, {
    "name": "Moines",
    "category": "catCambodge9",
    "categoryType": "Cambodge",
    "src": "cam-moines.jpg",
    "type": "Landscape"
}, {
    "name": "Riziculture",
    "category": "catCambodge10",
    "categoryType": "Cambodge",
    "src": "cam-riziere.jpg",
    "type": "Landscape"
}, {
    "name": "Monkey Family",
    "category": "catCambodge11",
    "categoryType": "Cambodge",
    "src": "cam-singe1.jpg",
    "type": "Landscape"
}, {
    "name": "Tuk-tuk and monkey",
    "category": "catCambodge12",
    "categoryType": "Cambodge",
    "src": "cam-singe2.jpg",
    "type": "Landscape"
}, {
    "name": "Angkor",
    "category": "catCambodge13",
    "categoryType": "Cambodge",
    "src": "cam-temple1.jpg",
    "type": "Landscape"
}, {
    "name": "Fresques",
    "category": "catCambodge14",
    "categoryType": "Cambodge",
    "src": "cam-temple2.jpg",
    "type": "Landscape"
}, {
    "name": "Angkor",
    "category": "catCambodge15",
    "categoryType": "Cambodge",
    "src": "cam-temple3.jpg",
    "type": "Landscape"
}, {
    "name": "Temple",
    "category": "catCambodge16",
    "categoryType": "Cambodge",
    "src": "cam-temple5.jpg",
    "type": "Landscape"
}, {
    "name": "Angkor",
    "category": "catCambodge17",
    "categoryType": "Cambodge",
    "src": "cam-temple6.jpg",
    "type": "Portrait"
}, {
    "name": "Good View",
    "category": "catCambodge18",
    "categoryType": "Cambodge",
    "src": "cam-temple-montagne.jpg",
    "type": "Landscape"
}, {
    "name": "Escalier menant au Mékong",
    "category": "catLaos1",
    "categoryType": "Laos",
    "src": "lao-dragon-et-mekong.jpg",
    "type": "Landscape"
}, {
    "name": "Rencontre amicale",
    "category": "catLaos2",
    "categoryType": "Laos",
    "src": "lao-elephant.jpg",
    "type": "Landscape"
}, {
    "name": "Démo de Boxe Thaï",
    "category": "catLaos3",
    "categoryType": "Laos",
    "src": "lao-festival-sainyabouli.jpg",
    "type": "Landscape"
}, {
    "name": "Festival de Sayabouri",
    "category": "catLaos4",
    "categoryType": "Laos",
    "src": "lao-flag.jpg",
    "type": "Landscape"
}, {
    "name": "Frontière",
    "category": "catLaos5",
    "categoryType": "Laos",
    "src": "lao-frontiere-laos.jpg",
    "type": "Landscape"
}, {
    "name": "Bord du Mékong",
    "category": "catLaos6",
    "categoryType": "Laos",
    "src": "lao-islands.jpg",
    "type": "Portrait"
}, {
    "name": "Karaoké familial",
    "category": "catLaos7",
    "categoryType": "Laos",
    "src": "lao-karaoke.jpg",
    "type": "Landscape"
}, {
    "name": "Hors des sentiers battus",
    "category": "catLaos8",
    "categoryType": "Laos",
    "src": "lao-nature1.jpg",
    "type": "Landscape"
}, {
    "name": "Petit rafraichissement",
    "category": "catLaos9",
    "categoryType": "Laos",
    "src": "lao-nature2.jpg",
    "type": "Landscape"
}, {
    "name": "Barrière d’obus",
    "category": "catLaos10",
    "categoryType": "Laos",
    "src": "lao-obus-phonsavan.jpg",
    "type": "Landscape"
}, {
    "name": "Paradis perdu",
    "category": "catLaos11",
    "categoryType": "Laos",
    "src": "lao-rafi-on-the-lake.jpg",
    "type": "Landscape"
}, {
    "name": "Belle rencontre",
    "category": "catLaos12",
    "categoryType": "Laos",
    "src": "lao-rire.jpg",
    "type": "Landscape"
}, {
    "name": "Masha-Urban-Lili et Rafi",
    "category": "catLaos13",
    "categoryType": "Laos",
    "src": "lao-team-en-velo.jpg",
    "type": "Landscape"
}, {
    "name": "Retour en enfance !",
    "category": "catLaos14",
    "categoryType": "Laos",
    "src": "lao-team-et-enfants.jpg",
    "type": "Landscape"
}, {
    "name": "Cité à découvrir",
    "category": "catLaos15",
    "categoryType": "Laos",
    "src": "lao-temple1.jpg",
    "type": "Landscape"
}, {
    "name": "Les joies du deux-roues",
    "category": "catLaos16",
    "categoryType": "Laos",
    "src": "lao-trip-en-velo.jpg",
    "type": "Landscape"
}, {
    "name": "Baie d’Along",
    "category": "catVietnam1",
    "categoryType": "Vietnam",
    "src": "vie-baie-d-along.jpg",
    "type": "Landscape"
}, {
    "name": "Baie d’Along",
    "category": "catVietnam2",
    "categoryType": "Vietnam",
    "src": "vie-jonques.jpg",
    "type": "Landscape"
}, {
    "name": "Barbecue improvisé",
    "category": "catVietnam3",
    "categoryType": "Vietnam",
    "src": "vie-lili-barbecue.jpg",
    "type": "Landscape"
}, {
    "name": "Village flottant",
    "category": "catVietnam4",
    "categoryType": "Vietnam",
    "src": "vie-marche-flottant.jpg",
    "type": "Landscape"
}, {
    "name": "Mausolée d’Ho Chi Minh",
    "category": "catVietnam5",
    "categoryType": "Vietnam",
    "src": "vie-mausole-d-ho-chi-min.jpg",
    "type": "Landscape"
}, {
    "name": "Culture en terrasse",
    "category": "catVietnam6",
    "categoryType": "Vietnam",
    "src": "vie-mong-montagne.jpg",
    "type": "Landscape"
}, {
    "name": "Artiste de rue",
    "category": "catVietnam7",
    "categoryType": "Vietnam",
    "src": "vie-peintre-de-rue.jpg",
    "type": "Landscape"
}, {
    "name": "Calumet de la paix",
    "category": "catVietnam8",
    "categoryType": "Vietnam",
    "src": "vie-soiree-mong.jpg",
    "type": "Landscape"
}, {
    "name": "Portes de la cité",
    "category": "catVietnam9",
    "categoryType": "Vietnam",
    "src": "vie-temple.jpg",
    "type": "Landscape"
}, {
    "name": "Les joies du deux-roues !",
    "category": "catVietnam10",
    "categoryType": "Vietnam",
    "src": "vie-transport.jpg",
    "type": "Landscape"
}, {
    "name": "Tout est bon dans le cochon !",
    "category": "catVietnam11",
    "categoryType": "Vietnam",
    "src": "vie-village.jpg",
    "type": "Landscape"
}, {
    "name": "City Center",
    "category": "catVietnam12",
    "categoryType": "Vietnam",
    "src": "vie-ville.jpg",
    "type": "Landscape"
}, {
    "name": "Partie de foot improvisée",
    "category": "catVietnam13",
    "categoryType": "Vietnam",
    "src": "vie-foot.jpg",
    "type": "Landscape"
}, {
    "name": "Cours d’eau enchanté",
    "category": "catVietnam14",
    "categoryType": "Vietnam",
    "src": "vie-paysage1.jpg",
    "type": "Landscape"
}, {
    "name": "Jungle démesurée",
    "category": "catVietnam15",
    "categoryType": "Vietnam",
    "src": "vie-paysage2.jpg",
    "type": "Landscape"
}, {
    "name": "Cueillette d’oranges",
    "category": "catAustralie1",
    "categoryType": "Australie",
    "src": "aus-6.jpg",
    "type": "Landscape"
}, {
    "name": "Surplombant Gayndah",
    "category": "catAustralie2",
    "categoryType": "Australie",
    "src": "aus-7.jpg",
    "type": "Landscape"
}, {
    "name": "Daintree National Park",
    "category": "catAustralie3",
    "categoryType": "Australie",
    "src": "aus-beach.jpg",
    "type": "Landscape"
}, {
    "name": "Bivouac en bonne compagnie",
    "category": "catAustralie4",
    "categoryType": "Australie",
    "src": "aus-big-goana.jpg",
    "type": "Landscape"
}, {
    "name": "Craddle Mountains (Tasmanie)",
    "category": "catAustralie5",
    "categoryType": "Australie",
    "src": "aus-cradlle-mountain.jpg",
    "type": "Landscape"
}, {
    "name": "Trek dans le bush",
    "category": "catAustralie6",
    "categoryType": "Australie",
    "src": "aus-creek.jpg",
    "type": "Landscape"
}, {
    "name": "Devil Marble",
    "category": "catAustralie7",
    "categoryType": "Australie",
    "src": "aus-devil-marble1.jpg",
    "type": "Landscape"
}, {
    "name": "Devil Marble",
    "category": "catAustralie8",
    "categoryType": "Australie",
    "src": "aus-devil-marble2.jpg",
    "type": "Landscape"
}, {
    "name": "Elery Creek",
    "category": "catAustralie9",
    "categoryType": "Australie",
    "src": "aus-elery-creek.jpg",
    "type": "Landscape"
}, {
    "name": "Outback",
    "category": "catAustralie10",
    "categoryType": "Australie",
    "src": "aus-fire.jpg",
    "type": "Portrait"
}, {
    "name": "Indian Head",
    "category": "catAustralie11",
    "categoryType": "Australie",
    "src": "aus-frazer-island1.jpg",
    "type": "Portrait"
}, {
    "name": "Fraser Island",
    "category": "catAustralie12",
    "categoryType": "Australie",
    "src": "aus-frazer-island2.jpg",
    "type": "Landscape"
}, {
    "name": "Berni-Niki-Rafi-Lili",
    "category": "catAustralie13",
    "categoryType": "Australie",
    "src": "aus-frazer-island3.jpg",
    "type": "Landscape"
}, {
    "name": "Pêche avec Marc",
    "category": "catAustralie14",
    "categoryType": "Australie",
    "src": "aus-gayndah.jpg",
    "type": "Landscape"
}, {
    "name": "Remember us",
    "category": "catNouvelleZelande1",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-barbecue.jpg",
    "type": "Landscape"
}, {
    "name": "Bivouac en compagnie de Laura et Lili",
    "category": "catNouvelleZelande2",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-camping.jpg",
    "type": "Landscape"
}, {
    "name": "Collines enchantées",
    "category": "catNouvelleZelande3",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-colline.jpg",
    "type": "Landscape"
}, {
    "name": "Lili en thining",
    "category": "catNouvelleZelande4",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-cueillette.jpg",
    "type": "Portrait"
}, {
    "name": "Dusky Dolphin",
    "category": "catNouvelleZelande5",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-dauphin.jpg",
    "type": "Landscape"
}, {
    "name": "Double arc en ciel",
    "category": "catNouvelleZelande6",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-double-rainbow.jpg",
    "type": "Landscape"
}, {
    "name": "Soirée de fin de saison",
    "category": "catNouvelleZelande7",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-fire-on-the-river.jpg",
    "type": "Landscape"
}, {
    "name": "Hobbiton",
    "category": "catNouvelleZelande8",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-hobitton.jpg",
    "type": "Landscape"
}, {
    "name": "Kea",
    "category": "catNouvelleZelande9",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-kea.jpg",
    "type": "Landscape"
}, {
    "name": "Blue Lake",
    "category": "catNouvelleZelande10",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-lac.jpg",
    "type": "Landscape"
}, {
    "name": "Lake Matheson",
    "category": "catNouvelleZelande11",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-lac2.jpg",
    "type": "Landscape"
}, {
    "name": "What’s the phoque",
    "category": "catNouvelleZelande12",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-phoque.jpg",
    "type": "Landscape"
}, {
    "name": "What’s the phoque",
    "category": "catNouvelleZelande13",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-queenstown-glenorchy.jpg",
    "type": "Landscape"
}, {
    "name": "Wanaka Legendary Tree",
    "category": "catNouvelleZelande14",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-wanaka-legendary-tree.jpg",
    "type": "Landscape"
}, {
    "name": "Lili en Woofing",
    "category": "catNouvelleZelande15",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-woofing.jpg",
    "type": "Landscape"
}, {
    "name": "Key Point",
    "category": "catNouvelleZelande16",
    "categoryType": "Nouvelle-Zélande",
    "src": "nou-key-point.jpg",
    "type": "Landscape"
}]

let arrayButton = [{
    "name": "all",
    "fNbImage": "0",
    "iNbImage": "array.length",
    "moreView": "moreViewAll",
    "copyright": ""
}, {
    "name": "cambodge",
    "fNbImage": "0",
    "iNbImage": "nbImgCambodge",
    "moreView": "moreViewCambodge",
    "copyright": "UrbanCerjak et LindaLustig"
}, {
    "name": "laos",
    "fNbImage": "nbImgCambodge",
    "iNbImage": "nbImgLaos",
    "moreView": "moreViewLaos",
    "copyright": "UrbanCerjak et LindaLustig"
}, {
    "name": "vietnam",
    "fNbImage": "nbImgLaos",
    "iNbImage": "nbImgVietnam",
    "moreView": "moreViewVietnam",
    "copyright": "UrbanCerjak et LindaLustig"
}, {
    "name": "australie",
    "fNbImage": "nbImgVietnam",
    "iNbImage": "nbImgAustralie",
    "moreView": "moreViewAustralie",
    "copyright": "LindaLustig"
}, {
    "name": "nouvelleZelande",
    "fNbImage": "nbImgAustralie",
    "iNbImage": "nbImgZelande",
    "moreView": "moreViewNouvelleZelande",
    "copyright": "CamilleMaye et LindaLustig"
}]

/* Number Image Category */

let imgCambodge = [];
let imgLaos = [];
let imgVietnam = [];
let imgAustralie = [];
let imgNouvelleZelande = [];
let imgError = [];

for (let i = 0; i < array.length; i++) {
    switch (array[i].categoryType) {
        case "Cambodge":
            imgCambodge.push(array[i].src);
            break;
        case "Laos":
            imgLaos.push(array[i].src);
            break;
        case "Vietnam":
            imgVietnam.push(array[i].src);
            break;
        case "Australie":
            imgAustralie.push(array[i].src);
            break;
        case "Nouvelle-Zélande":
            imgNouvelleZelande.push(array[i].src);
            break;
        default:
            imgError.push(array[i].src);
            break;
    }
}

let nbImgDefault = 10;
let nbImgCambodge = imgCambodge.length;
let nbImgLaos = nbImgCambodge + imgLaos.length;
let nbImgVietnam = nbImgLaos + imgVietnam.length;
let nbImgAustralie = nbImgVietnam + imgAustralie.length;
let nbImgZelande = nbImgAustralie + imgNouvelleZelande.length;

/* Display main default */

const main = document.querySelector('.containerFlex');
let result = '';
for (let i = 0; i < array.length; i++) {
    let name = array[i].name;
    let category = array[i].category;
    let src = array[i].src;
    let type = array[i].type;
    if (i < nbImgDefault) {
        var myMainContainerStart = '<a href="#"><div class="containerPoster ' + category + '">';
    } else {
        var myMainContainerStart = '<a href="#"><div class="containerPoster ' + category + ' catNoView">';
    }
    let myMainImg = '<a href="#"><img class="img' + type + '" src="../src/gallery/' + src + '" alt="poster"></a>';
    let myMainOverlay = '<div><a class="overlay overlay' + type + '" href="../src/gallery/' + src + '" target="_blank"><div><p>' + name + '</p></div></a></div>';
    let myMainContainerEnd = '</div></a>';
    result += myMainContainerStart + myMainImg + myMainOverlay + myMainContainerEnd;
}
let resultButton = [];
for (let i = 0; i < arrayButton.length; i++) {
    if (i == 0) {
        resultButton[i] = '<div class="informationGuide textCenter ' + arrayButton[i].moreView + '"><button class="">Plus de photos</button><div class="copyrights"><div>Copyright Australie : LindaLustig </div><div> Copyright Cambodge, Vietnam, Laos et Thailande : UrbanCerjak et LindaLustig </div><div>Copyright Nouvelle-Zélande: CamilleMaye et LindaLustig </div><div> Copyrigth Equteur et Pérou : CamilleMartre et PaulGrimal</div></div></div>';
    } else {
        resultButton[i] = '<div class="informationGuide textCenter catNoView ' + arrayButton[i].moreView + '"><button>Plus de photos</button><div class="copyrights"><div>Copyright : ' + arrayButton[i].copyright + '</div></div></div>';
    }
}
if (main && result) {
    let resultFinal = "";
    for (let i = 0; i < arrayButton.length; i++) {
        resultFinal += resultButton[i];
    }
    main.innerHTML = result + resultFinal;
}

window.addEventListener('DOMContentLoaded', (event) => {

    /* DOM select button */
    const all = document.querySelector('.all');
    const cambodge = document.querySelector('.cambodge');
    const laos = document.querySelector('.laos');
    const vietnam = document.querySelector('.vietnam');
    const australie = document.querySelector('.australie');
    const nouvelleZelande = document.querySelector('.nouvelleZelande');
    const moreViewAll = document.querySelector('.moreViewAll');
    const moreViewCambodge = document.querySelector('.moreViewCambodge');
    const moreViewLaos = document.querySelector('.moreViewLaos');
    const moreViewVietnam = document.querySelector('.moreViewVietnam');
    const moreViewAustralie = document.querySelector('.moreViewAustralie');
    const moreViewNouvelleZelande = document.querySelector('.moreViewNouvelleZelande');

    /* DOM select class image */
    let tab = [];
    for (let i = 0; i < array.length; i++) {
        /* Add class to the JSON */
        tab[i] = 'document.querySelector(\'.' + array[i].category + '\')';
    }

    /* DOM display button category */
    for (let j = 0; j < arrayButton.length; j++) {
        let buttonName = arrayButton[j].name;
        let iNbImage = arrayButton[j].iNbImage;
        let fNbImage = arrayButton[j].fNbImage;
        let moreView = arrayButton[j].moreView;
        eval(buttonName).addEventListener('click', () => {
            for (let i = 0; i < array.length; i++) {
                /* Hide image */
                eval(tab[i]).classList.add('catNoView');
            }
            for (let i = eval(fNbImage); i < eval(iNbImage); i++) {
                /* View image default nb */
                if (i < (eval(fNbImage) + nbImgDefault)) {
                    eval(tab[i]).classList.remove('catNoView');
                } else {
                    eval(tab[i]).classList.add('catNoView');
                }
                /* DOM select button moreView */
                document.querySelector('.moreViewAll').classList.add('catNoView');
                document.querySelector('.moreViewCambodge').classList.add('catNoView');
                document.querySelector('.moreViewLaos').classList.add('catNoView');
                document.querySelector('.moreViewVietnam').classList.add('catNoView');
                document.querySelector('.moreViewAustralie').classList.add('catNoView');
                document.querySelector('.moreViewNouvelleZelande').classList.add('catNoView');
                /* View All image */
                if (i >= (eval(fNbImage) + nbImgDefault)) {
                    document.querySelector('.' + moreView + '').classList.remove('catNoView');
                } else {
                    document.querySelector('.' + moreView + '').classList.add('catNoView');
                }
            }
        })
        eval(moreView).addEventListener('click', () => {
            for (let i = 0; i < array.length; i++) {
                /* Hide image */
                eval(tab[i]).classList.add('catNoView');
            }
            for (let i = eval(fNbImage); i < eval(iNbImage); i++) {
                /* View image */
                eval(tab[i]).classList.remove('catNoView');
                document.querySelector('.' + moreView + '').classList.add('catNoView');
            }
        })
    }
});