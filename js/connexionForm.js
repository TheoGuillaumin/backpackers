/* Select main connexion.html */
var formContainer = document.querySelector('#connexionFormGenerator');

function signIn() {

    /* HTML page sign in */
    var connexionFormGen = '<h2>Connexion</h2><form action="traitementSignIn.html" method="GET"><div><label for="mail">Adresse mail : </label><input type="email" name="mail" class="connexionFormInput" id="firstInput" required></div><div><label for="motDePasse">Mot de passe : </label><input type="password" name="password" id="lastInput" class="connexionFormInput" required></div><div><button type="submit" id="connexionFormButton">Connexion</button></div></form><div><p id="noRegister">Pas encore inscrit ? </p><a href="#" class="connexionFormLinks" onclick="signUp()"> Inscription</a></div><a href="#" class="connexionFormLinks" onclick="lostPassword()">Mot de passe oublié ?</a>';

    if (formContainer && connexionFormGen) {

        /* Page display sign in */
        formContainer.innerHTML = connexionFormGen;
    }

}

function signUp() {

    /* HTML page sign up */
    var signUpTitle = '<h2>Inscription</h2>';
    var signUpInputMail = '<form name = "signUp" action = "traitementSignUp.html" method = "GET"><div><div class="labelClass"><label for="mail" id="label1">Adresse mail : </label></div><input type="email" name="mail" class="connexionFormInput" id="firstInput" required></div>';
    var signUpInputLastName = '<div><div class="labelClass"><label for="name" id="label2">Nom : </label></div><input type="text" name="name" class="connexionFormInput" required></div>';
    var signUpInputFirstName = '<div><div class="labelClass"><label for="firstName" id="label3">Prénom : </label></div><input type="text" name="firstName" class="connexionFormInput" required></div>';
    var signUpInputPseudo = '<div><div class="labelClass"><label for="pseudo" id="label4">Pseudo : </label></div><input type="text" name="pseudo" class="connexionFormInput" required></div>';
    var signUpInputPassword = '<div><div class="labelClass"><label for="motDePasse" id="label5">Mot de passe : </label></div><input type="password" name="password" class="connexionFormInput" required></div>';
    var signUpInputPasswordConfirmation = '<div><div class="labelClass"><label for="motDePasseConf" id="label6">Mot de passe (Confirmation) : </label></div><input type="password" name="passwordConf" id="lastInput" class="connexionFormInput" required></div>';
    var signUpButton = '<div><button type="submit" id="connexionFormButton">Inscription</button></div></form>';
    var signUpLinks = '<div><p id="register">Déjà inscrit ? </p><a href="#" class="connexionFormLinks" onclick="signIn()"> Connexion</a></div>';
    var connexionFormGen = signUpTitle + signUpInputMail + signUpInputLastName + signUpInputFirstName + signUpInputPseudo + signUpInputPassword + signUpInputPasswordConfirmation + signUpButton + signUpLinks;

    if (formContainer && connexionFormGen) {

        /* Page display sign up */
        formContainer.innerHTML = connexionFormGen;
    }

}

function lostPassword() {

    /* HTML page lost password */
    var lostPasswordTitle = '<h2>Mot de passe oublié ?</h2>';
    var lostPasswordInputMail = '<form action="../pages/traitementPasswordForgotten.html" method="GET"><div id="lastInput"><label for="mail">Adresse mail : </label><input type="email" name="mail" class="connexionFormInput" id="firstInput"></div>';
    var lostPasswordButton = '<div><button type="submit" id="connexionFormButton">Confirmer</button></div></form>';
    var lostPasswordLinks = '<div><a href="#" class="connexionFormLinks" onclick="signIn()"> Connexion</a></div>';
    var lostPasswordFormGen = lostPasswordTitle + lostPasswordInputMail + lostPasswordButton + lostPasswordLinks;

    if (formContainer && lostPasswordFormGen) {

        /* Page display lost password */
        formContainer.innerHTML = lostPasswordFormGen;
    }

}

/* Default main */
signIn();