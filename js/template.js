function myHeaderFunction() {

    /* Header HTML */
    var myHeaderBackdrop = '<div class="burgerMenuBackdrop"></div><div class="burgerMenuClose"><i class="gg-close"></i></div>';
    var myHeaderLogo = '<!-- Website Logo --> <a href="index.html"><img src="../src/no.gif" id = "logo" alt = "logo"></a>';
    var myHeaderTitle = '<!-- Website Title --> <h1> Le monde des Backpackers </h1>';
    var myHeaderCatchphrase = '<!-- Website catchphrase --><div><p class = "pCatchphrase">Explore The Possibilities</p> </div>'
    var myHeaderNav = '<nav><!-- List Navbar --><ul><!-- Content Links Pictures --><li><div> <a href = "../pages/index.html"> Accueil </a></div><img id="traces" src = "../src/traces.png"> </li> <li><div> <a href = "../pages/gallery.html"> Galerie </a></div> <img id = "traces"src = "../src/traces.png"> </li> <li><div> <a href = "../pages/forum.html"> Forum </a></div> <img id="traces" src="../src/traces.png"> </li> <li><div class="dropdown"> <a href="#">Annexes</a></div> <img id = "traces"src = "../src/traces.png"><ul class = "sous"><li> <a href = "../pages/faq.html"> FAQ </a></li><li> <a href = "../pages/contact.html"> Contact </a></li> <li> <a href = "../pages/biography.html"> Biographie </a></li></ul> </li> <li><div><a href = "../pages/connexion.html"> Connexion </a> </div> <img id = "traces"src = "../src/traces.png"></li><li><div> <a href = "../pages/disconnection.html"> Déconnexion </a></div> <img id = "traces"src = "../src/traces.png"> </li></ul></nav>';
    var myHeaderBurgerMenu = '<div class="burgerMenuOpen"><i class="fas fa-bars"></i></div>';
    var myHeader = myHeaderBackdrop + myHeaderLogo + myHeaderTitle + myHeaderCatchphrase + myHeaderNav + myHeaderBurgerMenu;

    /* Header display */
    var headerContainer = document.querySelector('#myHeaderId');
    if (headerContainer && myHeader) {
        headerContainer.innerHTML = myHeader;
        window.addEventListener('DOMContentLoaded', (event) => {
            const nav = document.querySelector('nav');
            const burgerMenuOpen = document.querySelector('.burgerMenuOpen');
            const burgerMenuClose = document.querySelector('.burgerMenuClose');
            const burgerMenuBackdrop = document.querySelector('.burgerMenuBackdrop');

            burgerMenuOpen.addEventListener('click', () => {
                burgerMenuBackdrop.classList.add('backdrop');
                burgerMenuClose.classList.add('burgerMenuCloseView');
                nav.classList.add('ulView');
            });

            burgerMenuClose.addEventListener('click', () => {
                burgerMenuBackdrop.classList.remove('backdrop');
                burgerMenuClose.classList.remove('burgerMenuCloseView');
                nav.classList.remove('ulView');
            });
        });
    }
}

function myFooterFunction() {

    /* Footer HTML */
    var myFooterIcon = '<!-- Icon --><div class="footerClass"><div class="footerLogos"><a href="https://www.youtube.com/channel/UC0lBzNbuoXulTzcBUPWQW4A" target="_blank" rel="noopener noreferrer"><img src="../src/no.gif" alt="Logo + youtube\'s link" class="socialNetworkLogos"></a></div>';
    var myFooterLinks = '<!-- Links --><div class="footerContent"><div class="footerLinks"><a href="../pages/privacyPolicy.html" id="pdcId">Politique de confidentialité</a><a href="../pages/cgu.html" id="cguId">Conditions générales d\'utilisation</a></div></div></div>';
    var myFooter = myFooterIcon + myFooterLinks;

    /* Footer display */
    var footerContainer = document.querySelector('#myFooterId');
    if (footerContainer && myFooter) {
        footerContainer.innerHTML = myFooter;
    }

}

/* Display Header & Footer */
myHeaderFunction();
myFooterFunction();