// link sidebar
function sidebar() {
    var hideSidebar = '<div id="mySidenav" class="sidenav"><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>';
    var homeButton = '<a href="../home/adminHome.html">Accueil</a>';
    var bookButton = '<a href="../books/bookHome.html">Livres</a>';
    var forumButton = '<a href="../forum/forumHome.html">Forum</a>';
    var bioButton = '<a href="../biography/biographyHome.html">Biographie</a>';
    var galeryButton = '<a href="../galery/galeryHome.html">Galerie</a>';
    var faqButton = '<a href="../faq/faqHome.html">FAQ</a>';
    var newsletterButton = '<a href="../newsletter/newsletterSend.html">Newsletter</a>';
    var userButton = '<a href="../users/userHome.html">Utilisateurs (Coming Soon)</a>';
    var updateAdmin = '<a href="../home/accountUpdate.html" class="button">Modifier mon compte</a>';
    var disconect = '<a href="../../pages/index.html" class="button">Déconnexion</a></div></form></div>';
    var sidebarButtons = hideSidebar + homeButton + bookButton + forumButton + bioButton + galeryButton + faqButton + newsletterButton + userButton + updateAdmin + disconect;

    var sideBarContainer = document.querySelector('#adminSidebar');
    if (sideBarContainer && sidebarButtons) {
        sideBarContainer.innerHTML = sidebarButtons;
    }
};

// function for opened sidebar
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
};

// function for closed sidebar
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
};

sidebar();